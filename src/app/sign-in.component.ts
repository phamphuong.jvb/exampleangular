import { Component } from '@angular/core';
import { SignInServer } from './sign-in.server';


@Component({
  selector: 'app-signin',
  template: `<form (ngSubmit) = "onSubmit(formSignIn);" #formSignIn = "ngForm">
  <input placeholder="email" ngModel #txtEmail="ngModel" name="email"  required email/>
  <p *ngIf="txtEmail.touched && txtEmail.errors?.required">email is required</p>
  <p *ngIf="txtEmail.touched && txtEmail.errors?.email">email is not valid</p>
  <br>
  <input type="password"
   placeholder="password"
    name="password" 
    ngModel 
    #txtpassword = "ngModel"
    minlength="6"
    pattern="[a-z]*"
    >
  <br>
  <div ngModelGroup="Subjects">
  <label><input type="checkbox" [ngModel]="false" name="nodejs">nodejs<br></label>
  <label><input type="checkbox" [ngModel]="false" name="anguler">anguler<br></label>
  <label><input type="checkbox" [ngModel]="false" name="reactjs">reactjs<br></label>
  </div>
  <button [disabled] = "formSignIn.invalid">submit</button>
  </form>
  /* <button (click) = "postToExpress();">Post</button> 
  <p>{{ txtEmail.errors | json }}</p>
  <p>{{ txtpassword.errors | json }}</p>
  <p>{{ formSignIn.value | json }}</p>
  `,
  providers:[SignInServer]
  
})

export class SignInComponent {
 constructor(private signinServer:SignInServer){}

  onSubmit(formSignIn){
   this.signinServer.sendPost(formSignIn.value)
   .then(result => console.log(result))
   .catch(err => console.log(err));
  }


//   postToExpress(){
//       const url = 'http://localhost:3000/signin';
//       //return console.log(url);
//       const headers = new Headers({ 'Content-Type' : 'Content-Type'});
//       const body = JSON.stringify({ name: 'pham phuong'});
//       //return console.log(body);
//       this.http.post(url, body, {headers})
//       .toPromise()
//       .then(res => res.text())
//       .then(resText => console.log(resText));
//   }
}