import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PARAMETERS } from '@angular/core/src/util/decorators';
@Component({
  selector: 'app-contact-detail',
  template: `<a routerLink="/contact">contact</a><span style="margin-right:20px"></span>
  <a routerLink="/dashboard">dashboard</a>
  <router-outlet></router-outlet>
  <p>Ten:{{name}}</p><p>SDT:{{phoneNumber}}</p>
  `,
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {
  name = '';
  phoneNumber = '';
  constructor(private route : ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params : ParamMap) => {
      const id = params.get('id');
      this.name = params.get('name');
      this.phoneNumber = params.get('phoneNumber');
      //console.log(name,phoneNumber);
    });
  }

}
