import {Inject, Component, OnInit , OnChanges, SimpleChanges, Input } from '@angular/core';
import { UserService } from '../user.service';
import { NgForm} from '@angular/forms';
//import { Server, servers } from '../server.model';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';
import { HttpClient,HttpEventType } from '@angular/common/http';
import { Http} from '@angular/http';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  constructor(private user:UserService ,@Inject(LOCAL_STORAGE) private storage: WebStorageService ,private http: HttpClient) { }

  ngOnInit() {
  }
  public data:any=[];
 
  saveInLocal(key, val): void {
    console.log('recieved= key:' + key + 'value:' + val);
    this.storage.set(key, val);
    this.data[key]= this.storage.get(key);
    this.addWord();
   }
  getFromLocal(key): void {
    console.log('recieved= key:' + key);
    this.data[key]= this.storage.get(key);
    console.log(this.data);
   }
   //////////////////////////
   fileData: File = null;
  getBase64Image(img) {
    var canvas = document.createElement("canvas");
    //canvas.width = img.width;
    //canvas.height = img.height;

    // var ctx = canvas.getContext("2d");
    // ctx.drawImage(img, 0, 0);

    var dataURL = canvas.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
   fileProgress(fileInput: any) {
       this.fileData = <File>fileInput.target.files[0];
   }
  
  
    imgData= "";
    bannerImg="";
    onSubmit() {
       const formData = new FormData();
       formData.append('file', this.fileData);
      //  this.http.post('url/to/your/api', formData)
      //    .subscribe(res => {
      //      console.log(res);
      //      alert('SUCCESS !!');
      //    })
       
      //    this.http.post('url/to/your/api', formData, {
      //     reportProgress: true,
      //     observe: 'events'   
      // })
      // .subscribe(events => {
      //     if(events.type == HttpEventType.UploadProgress) {
      //         console.log('Upload progress: ', Math.round(events.loaded / events.total * 100) + '%');
      //     } else if(events.type === HttpEventType.Response) {
      //         console.log(events);
      //     }
      // });
      this.imgData = localStorage.getItem('imgData');
      this.imgData = this.getBase64Image(this.imgData);
      localStorage.setItem("imgData", this.imgData);
      
    
      //this.bannerImg = "data:image/png;base64," + this.imgData;
   }



////////////////////////////////////////////
 
  fill = "all";
  //userlist: Array<object> = [];
  arrWords = [
    { id: 1, en: 'action', vn: 'hành động', memorized: true },
    { id: 2, en: 'actor', vn: 'diễn viên', memorized: false },
    { id: 3, en: 'activity', vn: 'hoạt động', memorized: true },
    { id: 4, en: 'active', vn: 'chủ động', memorized: true },
    { id: 5, en: 'bath', vn: 'tắm', memorized: false },
    { id: 6, en: 'bedroom', vn: 'phòng ngủ', memorized: true }
  ];
  newEn="";
  newVN="";
  memorized = false;
 id= this.arrWords.length;

  addWord(){
    this.arrWords = JSON.parse(localStorage.getItem('arrWords'));
    this.arrWords = JSON.parse(sessionStorage.getItem('arrWords'));
    this.arrWords.unshift({
      id:this.arrWords.length+1,
      en:this.newEn,
      vn:this.newVN,
      memorized: this.memorized ? true : false
    })//unshift :đầu , push : cuối
 
    localStorage.setItem('arrWords', JSON.stringify(this.arrWords));
    sessionStorage.setItem('arrWords', JSON.stringify(this.arrWords));
    
    this.newEn = "";
    this.newVN = "";
    
  }; 
 
  //newJson:JSON;
  onUpdate(id:number){
  
      const objIndex = this.arrWords.findIndex(a => a.id === id);
      const newlist = this.arrWords[objIndex];
      
      const updatedProjects = [
       //this.arrWords.slice(1, objIndex),
       newlist,
       //this.arrWords.slice(objIndex + 1),
      ];
      updatedProjects.push();
      
      console.log("original data=", this.arrWords);
      console.log("updated data=", updatedProjects );
     //localStorage.setItem('arrWords',JSON.stringify(this.arrWords));
    }

  getword(): any {
    return this.arrWords;
  }
 
  removeWord(id : number){
    this.arrWords = JSON.parse(localStorage.getItem('arrWords'));
    this.id = JSON.parse(localStorage.getItem('id'));
    const index = this.arrWords.findIndex(a => a.id === id);
    
    this.arrWords.splice(index, 1);
    localStorage.setItem('arrWords', JSON.stringify(this.arrWords));
  };
  getshowstart(memorized:boolean){
    const dk1 = this.fill === "all";
    const dk2 = this.fill === "da" && memorized;
    const dk3 = this.fill === "chua" && !memorized;
    return dk1 || dk2 || dk3;
  }
 
 
}
