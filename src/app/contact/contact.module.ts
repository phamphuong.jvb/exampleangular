import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from "./contact.component";
import { CommonModule } from '@angular/common';
import { AuthguardGuard } from '../authguard.guard';
const routesConfig : Routes = [
    {path:'contact',canActivate: [AuthguardGuard], component : ContactComponent}
];
 @NgModule({
     imports : [
        CommonModule,
        RouterModule.forChild(routesConfig)
    ],
     declarations:[ContactComponent],
     providers:[AuthguardGuard]
 })

 export class ContactModule{

 }