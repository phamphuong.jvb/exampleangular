import { Component , OnInit} from '@angular/core';
import { FormGroup, FormControl, FormBuilder , Validator, Validators } from '@angular/forms';


@Component({
  selector: 'app-signup',
  template: `<form (ngSubmit) = "onSubmit();" [formGroup]="formSignUp">
  <input placeholder="email" formControlName = "email"/>
  <br>
  <p *ngIf="formSignUp.get('email').invalid && formSignUp.get('email').touched">email is required</p>
  <input type="password" placeholder="password" formControlName = "password" />
  <br>
  <div formGroupName="subject">
  <label><input type="checkbox" name="nodejs" formControlName = "nodeJs">nodejs</label>
  <label><input type="checkbox" name="anguler" formControlName = "Anguler">anguler</label>
  <label><input type="checkbox" name="reactjs" formControlName = "reactJs">reactjs</label>
  </div>
  <button [disabled] = "formSignUp.invalid">submit</button>
  </form>
  <code>{{ formSignUp.controls.email.errors | json }}</code>`
  
})

export class SignUpComponent implements OnInit{
    formSignUp : FormGroup;
    constructor(private fb:FormBuilder){}
    ngOnInit() :void{
        this.formSignUp = this.fb.group({
            email : ['', [Validators.required,gmailValidators]],
            password : '',
            subject : this.fb.group({
                nodeJs : false,
                Anguler : true,
                reactJs : false
                
            })
           
        })

    }
    onSubmit(){
        console.log(this.formSignUp.value);
    }
}

function gmailValidators(formControl : FormControl){
    if(formControl.value.includes("@gmail.com")){
        return null;
    }
    return { email :true};
}