import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private inUserLogin;
  private username;
  constructor() {
    this.inUserLogin = false;
   }
   setUserlogin(){
     this.inUserLogin = true;
   }
   getUserlogin(){
     return this.inUserLogin;
   }
}
