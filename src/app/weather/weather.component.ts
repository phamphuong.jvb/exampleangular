import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { WeatherService } from './weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css'],
  //providers:[WeatherService]
})
export class WeatherComponent implements OnInit {
  txtcityName = "";
  cityName = "";
  temp = null;
  loading = false;
  constructor(private weatherService:WeatherService){
     
  }

  ngOnInit() {
  }

  getWeather(){
    this.weatherService.getTemp(this.txtcityName)
    .then(temp=>{
      this.cityName = this.txtcityName;
      this.temp = temp;
      this.loading = false;
      this.txtcityName = "";
      }).catch(err => {
        alert('cannot find your city');
        this.loading = false;
        this.cityName = "";
        this.txtcityName = "";
      })
  }
  getMessenger(){
    if(this.loading){
      return 'loading ...'
    }
    return this.cityName === '' ? 'weather is now' : (this.cityName + 'is now' + this.temp )  
  }

}
