import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import 'rxjs/add/operator/toPromise'

@Injectable()

export class WeatherService{
    constructor(private http:Http){}
    getTemp(cityName:string){
        return this.http.get('https://api.openweathermap.org/data/2.5/weather?appid=ff2600e89719fd5c8dd957d4f87c5be5&units=metric&q=' + cityName)
        .toPromise()
        .then(res=>res.json())
        .then(resJson => resJson.main.temp);
        //.catch(err => console.log(err));
    }
}