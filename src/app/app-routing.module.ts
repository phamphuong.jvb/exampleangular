import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ContactModule } from './contact/contact.module';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthguardGuard } from './authguard.guard';
import { WeatherComponent } from './weather/weather.component';
import { WeatherService } from './weather/weather.service';
const routerConfig:Routes = [
  {path:'login', component : LoginComponent},
  {path:'dashboard', component : DashboardComponent},
  {path:'weather',  component : WeatherComponent},
  {path:'detail/:id/:name/:phoneNumber', component : ContactDetailComponent},
  //{path:'', redirectTo: "/login" , pathMatch:'full'},
  {path:'**', component : PageNotFoundComponent}
]
// canActivate: [AuthguardGuard]

@NgModule({
  imports: [
    RouterModule.forRoot(routerConfig),
    ContactModule,
    CommonModule,
    FormsModule
  ],
  declarations :[
    LoginComponent,
    ContactDetailComponent,
    PageNotFoundComponent,
    DashboardComponent,
    WeatherComponent
  ],
  exports: [RouterModule],
  providers:[AuthguardGuard,WeatherService]

})
export class AppRoutingModule { }
